// Опишіть своїми словами що таке Document Object Model (DOM)
// Структуроване представлення сторінки, де кожний єлемент це об'єкт, і за допомогою функцій dom ми можемо
// до цих об'єктів звертатись і змінювати

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText вивовдить тільки текст,  innerHTML повертає текст разом з тегами

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// за допомогою методів пошуку елементу: getElementBy* та querySelector*. Також теоретично можно звернутись 
// напряму наприклад через назву id, але цей метод краще не використовувати, тому що треба буде слідкувати,
// що в JS не задати змінну з таким же ім'ям, бо в такому випадку звернення до елементу не буде працювати


// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let p =Array.from(document.getElementsByTagName(`p`));
p.forEach(el => {
    el.style.background = '#ff0000';
  }
);
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти 
// батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести 
// в консоль назви та тип нод.
console.log(document.getElementById("optionsList"))
console.log(document.getElementById("optionsList").parentElement)
let child=document.getElementById("optionsList").childNodes
child.forEach(el =>{
        console.log(el.nodeName)
        console.log(el.nodeType)
}
);
// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
let cls=document.getElementById('testParagraph')
cls.textContent="This is a paragraph"
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.
let chld = document.body.querySelector('.main-header')
console.log(chld.children)
Array.from(chld.children).forEach(el =>{
  el.classList.add("nav-item")
})
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
Array.from(document.getElementsByClassName('section-title')).forEach(el =>{
  el.classList.remove('section-title')
})





